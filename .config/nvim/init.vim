let mapleader =","

if ! filereadable(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim"'))
	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim
	autocmd VimEnter * PlugInstall
endif

call plug#begin(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/plugged"'))
"Plug 'tpope/vim-surround'
"Plug 'preservim/nerdtree'
"Plug 'junegunn/goyo.vim'
"Plug 'PotatoesMaster/i3-vim-syntax'
"Plug 'jreybert/vimagit'
"Plug 'lukesmithxyz/vimling'
Plug 'https://git.dotya.ml/dalahast/deadkeys.git'
"Plug 'vimwiki/vimwiki'
Plug 'bling/vim-airline'
Plug 'tpope/vim-commentary'
"Plug 'kovetskiy/sxhkd-vim'
Plug 'ap/vim-css-color'
call plug#end()

" Enable system clipboard
    set clipboard+=unnamedplus

" Indent using 4 spaces
    set tabstop=4
    set softtabstop=-1
    set shiftwidth=0
    set shiftround
    set expandtab
    set autoindent
    set smartindent
" Split from bottom right
    set splitbelow splitright

" nnoremap c "_c
set nocompatible
filetype plugin on
syntax on
set encoding=utf-8

" Line numbering relative to cursor
    set number relativenumber

" Enable autocompletion:
    set wildmode=longest,list,full

" Disable automatic commenting on newline:
    autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" vimling:
	nm <leader>d :call ToggleDeadKeys()<CR>
	imap <leader>d <esc>:call ToggleDeadKeys()<CR>a

" Compile tex document
    map <leader>c <esc>:w! \| !lualatex %<CR>

" Runs a script that cleans out tex build files whenever I close out of a .tex file.
	autocmd VimLeave *.tex !texclear %

" Ensure tex files are read as tex
    autocmd BufRead,BufNewFile *.tex set filetype=tex

" Save as sudo for files that require it
    cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!

" Automatically deletes all trailing whitespace and newlines at end of file on save.
	autocmd BufWritePre * %s/\s\+$//e
	autocmd BufWritepre * %s/\n\+\%$//e
