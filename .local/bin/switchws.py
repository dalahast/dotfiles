#!/usr/bin/python

import sys
from i3ipc import Connection

PRIMARY = "eDP-1"

class Switcher():
    def __init__(self, connection):
        self.sway = connection
        if len(sys.argv) > 1:
            self.ws = sys.argv[1]
        else:
            raise TypeError("At least one argument is required")

    def find_focused_out(self):
        outputs = self.sway.get_outputs()
        for output in outputs:
            if output.focused:
                focused_out = output.name
        return focused_out

    def move_to_next(self):
        focused_out = self.find_focused_out()
        if focused_out == PRIMARY:
            self.sway.command("move container to workspace {}".format(self.ws + "s"))
        else:
            self.sway.command("move container to workspace {}".format(self.ws))

    def move_to(self):
        focused_out = self.find_focused_out()
        if focused_out == PRIMARY:
            self.sway.command("move container to workspace {}".format(self.ws))
        else:
            self.sway.command("move container to workspace {}".format(self.ws + "s"))

    def switch_ws(self):
        focused_out = self.find_focused_out()
        if focused_out == PRIMARY:
            self.sway.command("workspace {}".format(self.ws))
        else:
            self.sway.command("workspace {}".format(self.ws + "s"))

    def switch_other_ws(self):
        focused_out = self.find_focused_out()
        if focused_out == PRIMARY:
            self.sway.command("workspace {}".format(self.ws + "s"))
        else:
            self.sway.command("workspace {}".format(self.ws))

def main():
    switcher = Switcher(Connection())
    if len(sys.argv) > 2:
        flag = sys.argv[2]
    else:
        flag = None

    if not flag:
        switcher.switch_ws()
    if flag == "-m":
        switcher.move_to()
    if flag == "-n":
        switcher.move_to_next()
    if flag == "-o":
        switcher.switch_other_ws()
    else:
        raise ValueError("Unknown operation")

if __name__ == "__main__":
    main()
